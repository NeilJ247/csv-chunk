<?php

class CSVChunk {
  
  const CHUNK_DIR_POSTFIX = '.chunks';
  const CHUNK_PREFIX = 'chunk-';
  const SPLIT_COMMAND = 'split';

  private $filePath;
  private $chunkSize;
  private $chunkDirectory;

  /**
   * Constructor which takes a file and the size of chunks (lines) to split a CSV file into.
   * @param string  $filePath       this should be a CSV file
   * @param int     $chunkSize   default is 1000 lines
   */
  public function __construct($filePath, $chunkSize = 1000) {
    
    $validate = $this->validateFile($filePath);

    if ($validate !== true) {
      $this->log($validate);
      throw new Exception($validate, 1);
    }

    $this->setFilePath($filePath);
    $this->setChunkSize($chunkSize);
  }

  /**
   * The setter for the CSV file path we will be chunking.
   * @param string $filePath
   */
  public function setFilePath($filePath) {
    $this->filePath = $filePath;
  }

  /**
   * The getter for the CSV file path we will be chunking
   * @return string  returns the file.
   */
  public function getFilePath() {
    return $this->filePath;
  }

  /**
   * The setter for the no. of lines per chunk will be.
   * @param int $chunkSize   Number of lines per chunk will be.
   */
  public function setChunkSize($chunkSize) {
    $this->chunkSize = $chunkSize;
  }

  /**
   * The getter of how large per chunk size should be.
   * @return int   Number of lines per chunk
   */
  public function getChunkSize() {
    return $this->chunkSize;
  }

  /**
   * Private setter for chunk directory because we don't want the outside world
   * to alter it.
   * @param string $chunkDirectory  The directory where we will be saving chunk files
   */
  private function setChunkDirectory($chunkDirectory) {
    $this->chunkDirectory = $chunkDirectory;
  }

  /**
   * Returns the location where our chunked CSV files are.
   * @return string  The path of the chunk directory
   */
  public function getChunkDirectory() {
    return $this->chunkDirectory;
  }

  /**
   * Calls our validation functions to check if the file exists and is a CSV file.  
   * @param  string  $filePath  The file being validated 
   * @return mixed              Returns an error string if the file fails validation checks.  Else returns true.
   */
  private function validateFile($filePath) {
    if (!$this->doesFileExist($filePath)) {
      return 'File ' . $filePath . ' does not exist';
    }

    if (!$this->isCSVFile($filePath)) {
      return 'File ' . $filePath . ' is not a CSV file.';
    }

    return true;
  }

  /**
   * Gets the name of the CSV being chunked without the path.
   * @return string        The name of the file
   */
  private function getFileName() {
    $fileInfo = pathinfo($this->getFilePath());

    return $fileInfo['basename'];
  }

  /**
   * Checks if the CSV file exists and is readble.
   * @param  string $filePath  The CSV file
   * @return boolean           Returns the result from php native function is_readable
   */
  private function doesFileExist($filePath) {
    return is_readable($filePath);
  }

  /**
   * Checks if the file is a CSV file based on extension.  This could/should have more checks as 
   * extension is not the most reliable way checking what a file is.
   * @param  string  $file  The "CSV" file
   * @return boolean
   */
  private function isCSVFile($filePath) {
    $fileInfo = pathinfo($filePath);

    if ($fileInfo['extension'] == "csv" || $fileInfo['extension'] == "CSV") {
      return true;
    }  

    return false;
  }

  /**
   * "Sloth love Chunk!"
   *
   * Chunk the file using the 'split' command into the chunk directory.
   * @return boolean  true if the csv file has been split into chunks.  if not then false.
   */
  public function chunkFile() {

    $chunkDirectory = $this->createChunkDirectory();

    $command = sprintf("%s -de -l %d %s %s", 
                        self::SPLIT_COMMAND, 
                        $this->getChunkSize(), 
                        escapeshellarg($this->getFilePath()),
                        escapeshellarg($chunkDirectory . DIRECTORY_SEPARATOR . self::CHUNK_PREFIX . $this->getFileName() . '-')
                        );

    exec($command, $output = array(), $retval);

    if ($retval === 0) {
      $this->log('Created the following chunk files for ' . 
                  $this->getFilePath() . ": " . PHP_EOL . print_r($this->getChunkFiles(), true));
      return true;
    }

    return false;

  }

  /**
   * Get all of the chunked files from the chunk directory
   * @return mixed returns an array of chunked of files.  Returns false if the chunk directory does not 
   * exist. 
   */
  public function getChunkFiles() {
    $chunkDirectory = $this->getChunkDirectory();

    if (is_dir($chunkDirectory)) {
      $chunkFiles = scandir($chunkDirectory);
      array_shift($chunkFiles); // remove .
      array_shift($chunkFiles); // remove ..
      return $chunkFiles;
    }

    return false;
  }

  /**
   * Creates a directory where we will be storing our chunk files.
   * @return string  $chunkDirectory  The path of where we will store chunk files.
   */
  private function createChunkDirectory() {

      $chunkDirectory = $this->getFilePath() . self::CHUNK_DIR_POSTFIX;

      $this->log('Creating chunk directory with path ' . $chunkDirectory . '.');
      
      if (!is_dir($chunkDirectory)) {
        $dirCreated = mkdir($chunkDirectory);

        if ($dirCreated) {
          $this->log('Created chunk directory ' . $chunkDirectory . '.');
        } else {
          $errorMessage = 'Could not create chunk directory ' . $chunkDirectory . '.';
          $this->log($errorMessage);
          throw new Exception($errorMessage, 1);          
        }

        $this->setChunkDirectory($chunkDirectory);
      }

      return $chunkDirectory;
  }

  /**
   * Remove the chunk directory.  Should be called once we have finished with the chunked files.
   */
  public function cleanUp() {
    $chunkDirectory = $this->getChunkDirectory();
    
    // Should we be doing better check? 
    if (is_dir($chunkDirectory)) {
      $this->log('Removing chunk directory ' . $chunkDirectory . '.');
      exec("rm -rf " . $chunkDirectory);
    }
  }

  /**
   * Function to log the progress of chunking.  Useful for debuging issues.
   * @param  String  $message  The message we want to log
   */
  private function log($message) {
    $logFile = "/tmp/csvchunk.log";
    
    $dateTime = date('Y-m-d H:i:s');

    $message = sprintf('%s %s' . PHP_EOL,
                       $dateTime,
                       $message 
                      );   

    file_put_contents($logFile, $message, FILE_APPEND);
  }

}