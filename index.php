<?php
require_once('CSVChunk.class.php');


try {

  $csvChunker = new CSVChunk("largefile.csv");
  if ($csvChunker->chunkFile()) {
    echo "File has been chunked.\n";
  } else {
    echo "We didn't create chunks\n";
  }

  //$csvChunker->cleanUp();
} catch (Exception $ex) {

  echo $ex;

}
?>